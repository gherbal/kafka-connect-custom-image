ARG IMAGE_TAG
FROM confluentinc/cp-kafka-connect:6.0.1

USER root

RUN curl -L https://search.maven.org/remotecontent?filepath=com/blueapron/kafka-connect-protobuf-converter/2.2.1/kafka-connect-protobuf-converter-2.2.1-jar-with-dependencies.jar -o /usr/share/java/kafka-serde-tools/kafka-connect-protobuf-converter.jar
COPY SpamAccountOuterClass.java /opt/spam-account-proto/
RUN curl -L https://search.maven.org/remotecontent?filepath=com/google/protobuf/protobuf-java/3.10.0/protobuf-java-3.10.0.jar -o /opt/spam-account-proto/protobuf-java-3.10.0.jar
RUN javac -cp ".:/opt/spam-account-proto/protobuf-java-3.10.0.jar" -d /opt/spam-account-proto /opt/spam-account-proto/SpamAccountOuterClass.java
RUN cd /opt/spam-account-proto/ && jar -cvf spam-account-proto.jar *.class
RUN mv /opt/spam-account-proto/*.jar /usr/share/java/kafka-serde-tools/

USER 1000
